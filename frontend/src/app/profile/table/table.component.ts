import { Component, OnInit, ViewChild, AfterViewInit,ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from '../model';
import { ProfileServiceService } from '../profile-service.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [ProfileServiceService]
})
export class TableComponent implements OnInit, AfterViewInit {

  // people: Person[];
  persons: Person[] = [
    {
      id: 0,
      email: '',
      first_name: '',
      last_name: '',
      password: '',
      is_active: true
    }
  ];
  sortedData: Person[];

  len
  displayedColumns: string[] = ['email', 'full_name', 'activate', 'actions'];
  dataSource = new MatTableDataSource<Person>();
  clickedRows = new Set<Person>();

  constructor(private api: ProfileServiceService, private router: Router, private cdr: ChangeDetectorRef,) { 
    this.sortedData = this.persons.slice();
   }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  ngOnInit(): void {
    this.getProfile();

    // this.sort.sort({ id: 'full_name', start: 'asc', disableClear: false });
    const sortState: Sort = {active: 'email', direction: 'asc'};
    this.sort.active = sortState.active;
    this.sort.direction = sortState.direction;
    this.sort.sortChange.emit(sortState);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getProfile = () => {
    this.api.getAllUsers().subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
        this.cdr.detectChanges();
        console.log(data);
        this.len = data.length;
        console.log(this.len);
      },
    )
  }

  profileClicked = (customer) => {
    console.log(customer);
    this.router.navigate(['profile/edit'], { state: { id: customer } });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  sortData(sort: Sort) {
    const data = this.persons.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'email':
          return compare(a.email, b.email, isAsc);
        case 'full_name':
          return compare(a.first_name, b.first_name, isAsc);
        case 'active':
          return compare(a.is_active, b.is_active, isAsc);
        default:
          return 0;
      }
    });
  }

}
function compare(a: number | String | Boolean | null, b: number | String | Boolean | null, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}