import { Component, OnInit } from '@angular/core';
import { ProfileServiceService } from '../profile-service.service';
import { FormBuilder, FormGroup } from '@angular/forms'
import { Person, Persons } from '../model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [ProfileServiceService]
})
export class AddComponent implements OnInit {
  people:Person[];
  person = new Persons();

  
  constructor(public api: ProfileServiceService) { }

  ngOnInit(): void {}

  addPerson() {
    this.api.addUser(this.person)
      .subscribe(data => {
        console.log(data)
      })
  }
}
