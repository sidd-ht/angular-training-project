import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileServiceService } from '../profile-service.service';
import { Person, Persons } from '../model';

@Component({
  selector: 'app-edit-delete',
  templateUrl: './edit-delete.component.html',
  styleUrls: ['./edit-delete.component.css'],
  providers: [ProfileServiceService]
})
export class EditDeleteComponent implements OnInit {

  id = this.router.getCurrentNavigation().extras.state['id'];
  people:Person[];
  person = new Persons();

   constructor(private api: ProfileServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  editPerson() {
    this.api.editUser(this.person, this.id)
      .subscribe(data => {
        console.log(data)
      })
  }

  deletePerson() {
    this.api.deleteUser(this.id)
      .subscribe(data => {
        console.log(data)
      })
  }
}