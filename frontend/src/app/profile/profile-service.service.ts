import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Person } from './model';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    // Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProfileServiceService {

  baseurl = "http://127.0.0.1:8000";
  url: String = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.baseurl + '/customer/filter/', httpOptions)
  }

  getDetailUser(id): Observable<any[]> {
    return this.http.get<any[]>(this.baseurl + '/customer/filter/' + id + '/', httpOptions)
  }

  addUser(person: Person): Observable<Person[]> {
    const body = JSON.stringify(person);
    console.log(body)
    return this.http.post<Person[]>(this.baseurl + '/customer/filter/', body, httpOptions)
      .pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ))
  }

  editUser(person: Person, id): Observable<Person[]> {
    const body = JSON.stringify(person);
    console.log(body)
    return this.http.put<Person[]>(this.baseurl + '/customer/filter/' + id + '/', body, httpOptions)
      .pipe(
        catchError((err) => {
          console.error(err);
          throw err;
        }
        ))
  }


  deleteUser(id): Observable<any[]> {
    return this.http.delete<any[]>(this.baseurl + '/customer/filter/' + id + '/', httpOptions)
  }
}
