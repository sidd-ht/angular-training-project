import { Component, OnInit } from '@angular/core';
import { ProfileServiceService } from '../profile-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  providers: [ProfileServiceService]
})
export class CardComponent implements OnInit {
  customers = [ {id: ''},
                {email: ''},
                {first_name: ''},
                {last_name: ''},
                {is_active: ''},
                {profile_photo: ''}
              ];

  len

  constructor(private api: ProfileServiceService, private router: Router) { }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile = () => {
    this.api.getAllUsers().subscribe(
      data => {
        console.log(data);
        this.customers = data;
        this.len= data.length;
        console.log(this.len);
      },
    )
  }

  profileClicked = (customer) => {
    console.log(customer.id);
    this.router.navigate(['profile/edit'], { state: { id: customer.id } });
  }

}