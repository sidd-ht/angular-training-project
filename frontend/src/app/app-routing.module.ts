import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { CardComponent } from './profile/card/card.component';
import { TableComponent } from './profile/table/table.component';
import { AddComponent } from './profile/add/add.component';
import { EditDeleteComponent } from './profile/edit-delete/edit-delete.component';

const routes: Routes = [
  // { path: 'auth', component: AuthComponent },
  { 
    path: 'profile', 
    component: ProfileComponent,
    children: [
      {
      path: 'card-view',
      component: CardComponent
      },
      {
        path: 'table-view',
        component: TableComponent
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: 'edit',
        component: EditDeleteComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
