from rest_framework import routers
from .views import UserViewSet
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static


router = routers.DefaultRouter()
router.register(r'filter', UserViewSet)
# router.register(r'SaveFile$', save_file)


urlpatterns = [
    path('', include(router.urls))
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)